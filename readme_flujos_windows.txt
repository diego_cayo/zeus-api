1. Login
    1. Validar Login #-- colocar api
        - devuelve idLogin
        - devuelve idPersona
        - devuelve cuenta confirmada
    2. si cuenta confirmada 
        - es 0 se notifica que la cuenta aun no ha sido activada y se regresa al punto 1.6
        - es 1 se continua con el flujo
    2. Obtener datos Persona #-- colocar api
        - devuelve toda la data de la persona, incluyendo unidades ejecutoras y licencias. Asimismo, devuelve la data de las pc desde donde se conecta
        - la data de la PC recien traera valores una vez que se haya descargado el aplicativo y se haya logueado en una PC
    3. Se habilita descarga del proyecto

2. Levantar Aplicativo
    1. Se ingresa el email
    2. Email existe #-- colocar api
        - No Existe: Se notifica que el email no se encuentra registrado
        - Existe: continua flujo
    3. link del email redirecciona a pagina para setear contraseña #-- colocar api

3. Actualizar data Power BI
    1. Usuario debe estar logueado
    2. Actualizar password #-- colocar api

