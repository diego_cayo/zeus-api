1. Registrar Persona
    1. Cargar Parametros #-- GET http://localhost:4000/api/parametros/registropersona
        - combo tipo documento
        - combo unidades ejecutoras
        - combo licencias
    2. Persona Existe #-- GET http://localhost:4000/api/persona/existepersona/[idTipoDocumento]/[numeroDocumetno]/[email]
        - Si devuelve 0 se continua
        - Si devuelve 1 Notificar que la persona que se desea registrar ya existe, solo se permite 1 usuario por numero de documento y correo
    3. Insertar Persona #-- POST http://localhost:4000/api/persona {"Nombres": "Persona1","ApellidoP": "Persona1","ApellidoM": "Persona1","IdTipoDocumento": "1","NumeroDocumento": "222222222","Email": "Persona1@gmail.com"}
        - devuelve idPersona
    4. Insertar Unidades Ejecutoras y Licencia #-- POST http://localhost:4000/api/persona/licencia {"pIdUnidadEjecutora": 1,"pIdLicencia": 1,"pIdPersona": 1}
        - Se vincula la o las unidades ejecutoras sobre las que se le asignara una licencia y el tipo de licencia para cada una.
        - Se Genera Serial y se envia al correo 
        - 1 serial por cada combinacion UE / Licencia
    5. Inserta Login #-- POST http://localhost:4000/api/persona/login HTTP/1.1 {"pIdPersona": 1,"Usuario": "Usuario","Password": "password"}
        - Se registra el Usuario y Password
        - devuelve idLogin
    6. Se envia un email para validar el registro
        - el email contiene una url que al ingresar tiene un boton que ejecuta el siguiente punto
    7. Se activa la cuenta #-- GET http://localhost:4000/api/persona/login/[idlogin]
    
2. Login
    1. Validar Login #-- 
    2. Obtener Datos Login
        - devuelve idLogin
        - devuelve idPersona
        - devuelve cuenta confirmada
    2. si cuenta confirmada 
        - es 0 se notifica que la cuenta aun no ha sido activada y se regresa al punto 1.6
        - es 1 se continua con el flujo
    2. Obtener datos Persona #-- colocar api
        - devuelve toda la data de la persona, incluyendo unidades ejecutoras y licencias. Asimismo, devuelve la data de las pc desde donde se conecta
        - la data de la PC recien traera valores una vez que se haya descargado el aplicativo y se haya logueado en una PC
    3. Se habilita descarga del proyecto

3. Olvido Password
    1. Se ingresa el email
    2. Email existe #-- GET http://localhost:4000/api/persona/validarEmail/[email]
        - No Existe: Se notifica que el email no se encuentra registrado
        - Existe: continua flujo
    3. Se envia link al email 
    4. link para cambiar contraseña #-- PATCH  http://localhost:4000/api/persona/login/actualizarpassword

4. Cambiar Password
    1. Usuario debe estar logueado
    2. Se valida que el password ingresado sea el correspondiente para el usuario #-- POST http://localhost:4000/api/persona/login/esPassword
        - Si no corresponde se muestra alerta
        - Si corresponde se continua
    3. Actualizar password #-- PATCH  http://localhost:4000/api/persona/login/actualizarpassword

5. Registrar Orden Compra
