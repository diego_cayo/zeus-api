const respuestas = require('./respuesta')

function errors(err, req, res, next) {
    console.error('[err]', err);
    const message = err.message || 'Error Interno';
    const status = err.status || 500;
    respuestas.error(req, res, message, status);
}

module.exports = errors;