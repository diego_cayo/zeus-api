const express = require('express');
const funciones = require('./funciones')
const router = express.Router();

router.get('/', funciones.todos);
router.get('/todos', funciones.todos);
router.get('/:id', funciones.uno);
router.delete('/:id', funciones.eliminar)
router.patch('/', funciones.reactivar)
router.put('/', funciones.editar)
router.post('/', funciones.insertar)



module.exports = router;