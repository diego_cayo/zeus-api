const respuestas = require('../../red/respuesta');
const controlador = require('./index');

async function todos(req, res, next) {
    try {
        const items = await controlador.todos();
        respuestas.success(req, res, items, 200);
    } catch (err) {
        next(err);
    }
};

async function uno(req, res, next) {
    try {
        const item = await controlador.uno(req.params.id);
        respuestas.success(req, res, item, 200);
    } catch (err) {
        next(err);
    }
};

async function insertar(req, res, next) {
    try {
        const item = await controlador.insertar(req.body);
        respuestas.success(req, res, "Se Creo Satisfactoriamente el Registro", 200);
    } catch (err) {
        next(err);
    }
};

async function eliminar(req, res, next) {
    try {
        const item = await controlador.eliminar(req.params.id);
        respuestas.success(req, res, "Registro Eliminado Satisfactoriamente", 200);
    } catch (err) {
        next(err);
    }
};

async function editar(req, res, next) {
    try {
        const item = await controlador.editar(req.body);
        respuestas.success(req, res, "Se Edito Satisfactoriamente el Cliente", 200);
    } catch (err) {
        next(err);
    }
};

async function reactivar(req, res, next) {
    try {
        const item = await controlador.reactivar(req.body);
        respuestas.success(req, res, `Se reactivo correctamente el cliente`, 200);
    } catch (err) {
        next(err);
    }
};

module.exports = {
    todos,
    uno,
    insertar,
    eliminar,
    editar,
    reactivar
}