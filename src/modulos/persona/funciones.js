const respuestas = require('../../red/respuesta');
const controlador = require('./index');
const bcrypt = require('bcrypt');

async function validarEmail(req, res, next) {
    try {
        const item = await controlador.validarEmail(req.params.email);
        respuestas.success(req, res, item[0], 200);
    } catch (err) {
        next(err);
    }
};

async function existepersona(req, res, next) {
    try {
        const item = await controlador.existepersona(req.params.idtipodocumento, req.params.numerodocumento, req.params.email);
        respuestas.success(req, res, item[0], 200);
    } catch (err) {
        next(err);
    }
};

async function obtenerData(req, res, next) {
    try {
        const item = await controlador.obtenerData(req.params.idPersona);
        respuestas.success(req, res, item[0], 200);
    } catch (err) {
        next(err);
    }
};

async function insertarPersona(req, res, next) {
    try {
        const item = await controlador.insertarPersona(req.body);
        respuestas.success(req, res, item[0], 200);
    } catch (err) {
        next(err);
    }
};

async function insertarlicencias(req, res, next) {
    try {
        const item = await controlador.insertarlicencias(req.body);
        respuestas.success(req, res, item[0], 200);
    } catch (err) {
        next(err);
    }
};

async function crearLogin(req, res, next) {
    try {
        req.body.Password = await bcrypt.hash(req.body.Password, 5);
        const item = await controlador.crearLogin(req.body);
        respuestas.success(req, res, item[0], 200);
    } catch (err) {
        next(err);
    }
};

async function confirmarCuenta(req, res, next) {
    try {
        const item = await controlador.confirmarCuenta(req.params.idlogin);
        respuestas.success(req, res, item[0], 200);
    } catch (err) {
        next(err);
    }
};

async function esPassword(req, res, next) {
    try {
        const item = await controlador.esPassword(req.body);
        bcrypt.compare(req.body.old_Password, item[0][0].vPassword)
            .then(resultado => {
                respuestas.success(req, res, resultado, 200);
            })
    } catch (err) {
        next(err);
    }
};

async function actualizarPassword(req, res, next) {
    try {
        req.body.NewPassword = await bcrypt.hash(req.body.NewPassword, 5);
        //console.log(req.body);
        const item = await controlador.actualizarPassword(req.body);
        //console.log(item);
        respuestas.success(req, res, item[0], 200);
    } catch (err) {
        next(err);
    }
};




module.exports = {
    validarEmail,
    existepersona,
    insertarPersona,
    insertarlicencias,
    crearLogin,
    confirmarCuenta,
    esPassword,
    actualizarPassword,
    obtenerData
}