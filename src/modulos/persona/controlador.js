const tabla = 'Persona';

module.exports = function(dbInyectada) {
    let db = dbInyectada;
    if (!db) {
        db = require('../../db/zeus/mysql_persona');
    }

    function validarEmail(email) {
        return db.validarEmail(email)
    }

    function existepersona(idtipodocumento, numerodocumento, email) {
        return db.existepersona(idtipodocumento, numerodocumento, email)
    }

    function obtenerData(idPersona) {
        return db.obtenerData(idPersona)
    }

    function insertarPersona(body) {
        return db.insertarPersona(body)
    }

    function insertarlicencias(body) {
        return db.insertarlicencias(body)
    }

    function crearLogin(body) {
        return db.crearLogin(body)
    }

    function confirmarCuenta(idLogin) {
        return db.confirmarCuenta(idLogin)
    }

    function actualizarPassword(body) {
        return db.actualizarPassword(body)
    }

    function esPassword(body) {
        return db.esPassword(body)
    }

    return {
        validarEmail,
        existepersona,
        insertarPersona,
        insertarlicencias,
        crearLogin,
        confirmarCuenta,
        actualizarPassword,
        esPassword,
        obtenerData
    }
}