const express = require('express');
const funciones = require('./funciones')
const router = express.Router();

router.get('/validaremail/:email', funciones.validarEmail)
router.get('/obtenerData/:idPersona', funciones.obtenerData)
router.get('/existepersona/:idtipodocumento/:numerodocumento/:email', funciones.existepersona)
router.post('/', funciones.insertarPersona)
router.post('/licencia', funciones.insertarlicencias)
router.post('/login', funciones.crearLogin)
router.get('/login/:idlogin', funciones.confirmarCuenta)
router.post('/login/esPassword', funciones.esPassword)
router.patch('/login/actualizarpassword', funciones.actualizarPassword)



module.exports = router;