const respuestas = require('../../red/respuesta');
const controlador = require('./index');
const auth = require('../../auth');
const bcrypt = require('bcrypt');

async function login(req, res, next) {
    try {
        const item = await controlador.login(req.body);
        if (item[0][0] && item[0][0].vPassword) {
            bcrypt.compare(req.body.Password, item[0][0].vPassword)
                .then(resultado => {
                    //console.log(resultado);
                    if (resultado) {
                        respuestas.success(req, res, auth.asignarToken({...item }), 200);
                    } else {
                        respuestas.error(req, res, "Credenciales inválidas", 200);
                    }
                });
        } else {
            respuestas.error(req, res, "Usuario no existe", 200);
        }
    } catch (err) {
        next(err);
    }
};

module.exports = {
    login
}