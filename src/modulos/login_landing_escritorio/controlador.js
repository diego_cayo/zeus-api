const tabla = 'Login';

module.exports = function(dbInyectada) {
    let db = dbInyectada;
    if (!db) {
        db = require('../../db/zeus/mysql_login_escritorio');
    }

    function todos() {
        return db.todos(tabla)
    }

    function uno(id) {
        return db.uno(tabla, id)
    }

    function eliminar(id) {
        return db.eliminar(tabla, id)
    }

    function insertar(body) {
        return db.insertar(tabla, body)
    }

    function editar(body) {
        return db.editar(tabla, body)
    }

    function reactivar(body) {
        return db.reactivar(tabla, body)
    }

    return {
        todos,
        uno,
        eliminar,
        insertar,
        editar,
        reactivar
    }
}