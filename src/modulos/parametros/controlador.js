const tabla = 'Persona';

module.exports = function(dbInyectada) {
    let db = dbInyectada;
    if (!db) {
        db = require('../../db/zeus/mysql_parametros');
    }

    function registropersona() {
        return db.registropersona()
    }

    return {
        registropersona
    }
}