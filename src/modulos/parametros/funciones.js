const respuestas = require('../../red/respuesta');
const controlador = require('./index');

async function registropersona(req, res, next) {
    try {
        const item = await controlador.registropersona();
        respuestas.success(req, res, item[0], 200);
    } catch (err) {
        next(err);
    }
};

module.exports = {
    registropersona
}