const express = require('express');
const funciones = require('./funciones')
const seguridad = require('../../middleware/seguridad');
const router = express.Router();

router.get('/registropersona/', seguridad(), funciones.registropersona)

module.exports = router;