const express = require('express');
const morgan = require('morgan');
const cors = require('cors');

const config = require('./config');
const clientes = require('./modulos/clientes/rutas');
const loginWeb = require('./modulos/login_landing_web/rutas');
const loginEscritorio = require('./modulos/login_landing_escritorio/rutas');
const persona = require('./modulos/persona/rutas');
const parametros = require('./modulos/parametros/rutas');
const error = require('./red/errors');

const app = express();

app.use(cors());

app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));


app.set('port', config.app.port);

app.use('/api/clientes', clientes);
app.use('/api/loginweb', loginWeb);
app.use('/api/loginescritorio', loginEscritorio);
app.use('/api/persona', persona);
app.use('/api/parametros', parametros);


app.use(error);

module.exports = app;