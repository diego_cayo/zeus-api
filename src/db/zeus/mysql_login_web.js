const config = require('../../config');
const mysql = require('mysql')

const dbConfig = {
    host: config.mysql_zeus.host,
    port: config.mysql_zeus.port,
    user: config.mysql_zeus.user,
    password: config.mysql_zeus.password,
    database: config.mysql_zeus.database
}

let conexion;

function conMySql() {
    //console.log(dbConfig);
    conexion = mysql.createPool(dbConfig);
    conexion.on('error', (err) => {
        //console.log('[db error]', err);
        if (err.code === 'PROTOCOL_CONNECTION_LOST') {
            conMySql();
        } else {
            throw err;
        }
    })
}

conMySql();

function login(body) {
    return new Promise((resolve, reject) => {
        conexion.query(`CALL sp_valida_login (?)`, [body.Email], (err, res) => {
            return err ? reject(err) : resolve(res);
        });
    })
}

module.exports = {
    login
}