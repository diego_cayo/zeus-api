const config = require('../../config');
const mysql = require('mysql')

const dbConfig = {
    host: config.mysql_zeus.host,
    port: config.mysql_zeus.port,
    user: config.mysql_zeus.user,
    password: config.mysql_zeus.password,
    database: config.mysql_zeus.database
}

let conexion;

function conMySql() {
    //console.log(dbConfig);
    conexion = mysql.createPool(dbConfig);
    // conexion.connect((err) => {
    //     if (err) {
    //         console.log('[db error]', err);
    //         setTimeout(conMySql, 200);
    //     } else {
    //         console.log('MySql DB Conectada!!!');
    //     }
    // })
    conexion.on('error', (err) => {
        //console.log('[db error]', err);
        if (err.code === 'PROTOCOL_CONNECTION_LOST') {
            conMySql();
        } else {
            throw err;
        }
    })
}

conMySql();

function registropersona(body) {
    return new Promise((resolve, reject) => {
        conexion.query(`CALL sp_parametros_registro_persona ()`, (err, res) => {
            if (err) {
                return err
            } else {
                const response = {
                    error: false,
                    status: 200,
                    body: [{
                            cabecera: "Unidad Ejecutora",
                            data: []
                        },
                        {
                            cabecera: "Tipo Documento",
                            data: []
                        },
                        {
                            cabecera: "Licencias",
                            data: []
                        }
                    ]
                };

                res[0].forEach((row) => {
                    response.body[0].data.push({
                        idUnidadEjecutora: row.idUnidadEjecutora,
                        nCodUnidadEjecutora: row.nCodUnidadEjecutora,
                        vDscUnidadEjecutora: row.vDscUnidadEjecutora
                    });
                });

                res[1].forEach((row) => {
                    response.body[1].data.push({
                        IdTipoDocumento: row.IdTipoDocumento,
                        vDscTipoDocumento: row.vDscTipoDocumento
                    });
                });

                res[2].forEach((row) => {
                    response.body[2].data.push({
                        IdLicencia: row.IdLicencia,
                        vNombreLicencia: row.vNombreLicencia,
                        vDscLicencia: row.vDscLicencia,
                        nVigenciaMeses: row.nVigenciaMeses,
                        nPrecio: row.nPrecio
                    });
                });

                resolve(response);
            }
        });
    })
}

module.exports = {
    registropersona
}