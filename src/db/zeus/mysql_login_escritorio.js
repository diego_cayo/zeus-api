const config = require('../../config');
const mysql = require('mysql')

const dbConfig = {
    host: config.mysql_zeus.host,
    port: config.mysql_zeus.port,
    user: config.mysql_zeus.user,
    password: config.mysql_zeus.password,
    database: config.mysql_zeus.database
}

let conexion;

function conMySql() {
    //console.log(dbConfig);
    conexion = mysql.createPool(dbConfig);
    // conexion.connect((err) => {
    //     if (err) {
    //         console.log('[db error]', err);
    //         setTimeout(conMySql, 200);
    //     } else {
    //         console.log('MySql DB Conectada!!!');
    //     }
    // })
    conexion.on('error', (err) => {
        //console.log('[db error]', err);
        if (err.code === 'PROTOCOL_CONNECTION_LOST') {
            conMySql();
        } else {
            throw err;
        }
    })
}

conMySql();

function todos(tabla) {
    return new Promise((resolve, reject) => {
        conexion.query(`SELECT * FROM ${tabla}`, (err, res) => {
            return err ? reject(err) : resolve(res);
        });
    })
}

function uno(tabla, id) {
    return new Promise((resolve, reject) => {
        conexion.query(`SELECT * FROM ${tabla} WHERE Estado = 1 and Id = ${id}`, (err, res) => {
            return err ? reject(err) : resolve(res);
        });
    })
}

function editar(tabla, data) {
    return new Promise((resolve, reject) => {
        conexion.query(`UPDATE ${tabla} SET Nombre = ?, Edad = ?, Profesion = ? WHERE Estado = 1 and Id = ?`, [data.Nombre, data.Edad, data.Profesion, data.Id], (err, res) => {
            return err ? reject(err) : resolve(res);
        });
    })
}

function insertar(tabla, data) {
    return new Promise((resolve, reject) => {
        conexion.query(`INSERT INTO ${tabla} SELECT ?, ?, ?, ?, 1`, [data.Id, data.Nombre, data.Edad, data.Profesion], (err, res) => {
            return err ? reject(err) : resolve(res);
        });
    })
}

function eliminar(tabla, id) {
    return new Promise((resolve, reject) => {
        conexion.query(`UPDATE ${tabla} SET Estado = 0 WHERE Id = ${id}`, (err, res) => {
            return err ? reject(err) : resolve(res);
        });
    })
}

function reactivar(tabla, data) {
    return new Promise((resolve, reject) => {
        conexion.query(`UPDATE ${tabla} SET Estado = 1 WHERE Id = ?`, data.Id, (err, res) => {
            return err ? reject(err) : resolve(res);
        });
    })
}

module.exports = {
    todos,
    uno,
    insertar,
    eliminar,
    editar,
    reactivar
}