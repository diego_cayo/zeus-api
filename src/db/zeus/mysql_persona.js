const config = require('../../config');
const mysql = require('mysql')

const dbConfig = {
    host: config.mysql_zeus.host,
    port: config.mysql_zeus.port,
    user: config.mysql_zeus.user,
    password: config.mysql_zeus.password,
    database: config.mysql_zeus.database
}

let conexion;

function conMySql() {
    //console.log(dbConfig);
    conexion = mysql.createPool(dbConfig);
    conexion.on('error', (err) => {
        //console.log('[db error]', err);
        if (err.code === 'PROTOCOL_CONNECTION_LOST') {
            conMySql();
        } else {
            throw err;
        }
    })
}

conMySql();

function validarEmail(email) {
    return new Promise((resolve, reject) => {
        conexion.query(`CALL sp_existe_email ('${email}')`, (err, res) => {
            return err ? reject(err) : resolve(res);
        });
    })
}

function existepersona(pIdtipodocumento, numerodocumento, email) {
    return new Promise((resolve, reject) => {
        conexion.query(`CALL sp_existe_persona (${pIdtipodocumento},'${numerodocumento}','${email}')`, (err, res) => {
            return err ? reject(err) : resolve(res);
        });
    })
}

function obtenerData(pIdPersona) {
    return new Promise((resolve, reject) => {
        conexion.query(`CALL sp_obtener_data_login (${pIdPersona})`, (err, res) => {
            return err ? reject(err) : resolve(res);
        });
    })
}

function insertarPersona(body) {
    return new Promise((resolve, reject) => {
        conexion.query(`CALL sp_insertar_persona (?,?,?,?,?,?)`, [body.Nombres, body.ApellidoP, body.ApellidoM, body.pIdTipoDocumento, body.NumeroDocumento, body.Email], (err, res) => {
            return err ? reject(err) : resolve(res);
        });
    })
}

function insertarlicencias(body) {
    return new Promise((resolve, reject) => {
        conexion.query(`CALL sp_insertar_licencia_ue_persona (?,?,?)`, [body.pIdUnidadEjecutora, body.pIdLicencia, body.pIdPersona], (err, res) => {
            return err ? reject(err) : resolve(res);
        });
    })
}

async function crearLogin(body) {
    return new Promise((resolve, reject) => {
        conexion.query(`CALL sp_insertar_login (?,?)`, [body.pIdPersona, body.Password], (err, res) => {
            return err ? reject(err) : resolve(res);
        });
    })
}

function confirmarCuenta(pIdLogin) {
    return new Promise((resolve, reject) => {
        conexion.query(`CALL sp_confirmar_cuenta (${pIdLogin})`, (err, res) => {
            return err ? reject(err) : resolve(res);
        });
    })
}

function esPassword(body) {
    return new Promise((resolve, reject) => {
        conexion.query(`CALL sp_obtener_password (?)`, [body.Usuario], (err, res) => {
            return err ? reject(err) : resolve(res);
        });
    })
}

function actualizarPassword(body) {
    return new Promise((resolve, reject) => {
        //console.log(body.Usuario, body.NewPassword);
        conexion.query(`CALL sp_actualizar_password (?,?)`, [body.Usuario, body.NewPassword], (err, res) => {
            return err ? reject(err) : resolve(res);
        });
    })
}

module.exports = {
    validarEmail,
    existepersona,
    insertarPersona,
    insertarlicencias,
    crearLogin,
    confirmarCuenta,
    actualizarPassword,
    esPassword,
    obtenerData
}