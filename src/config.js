require('dotenv').config({ path: './.env' });
module.exports = {
    app: {
        port: process.env.PORT || 3000,
    },
    jwt: {
        secret: process.env.JET_SECRET || "TokenZeus",
        expiresIn: process.env.EXPIRESIN || "1h"
    },
    mysql_zeus: {
        host: process.env.MYSQL_HOST || 'localhost',
        port: process.env.MYSQL_PORT || 3306,
        user: process.env.MYSQL_USER_ZEUS || 'root',
        password: process.env.MYSQL_PASSWORD_ZEUS || '',
        database: process.env.MYSQL_DB_ZEUS || 'ejemplo'
    },
    mysql_zeus_bi_siaf: {
        host: process.env.MYSQL_HOST || 'localhost',
        port: process.env.MYSQL_PORT || 3306,
        user: process.env.MYSQL_USER_ZEUS_BI_SIAF || 'root',
        password: process.env.MYSQL_PASSWORD_ZEUS_BI_SIAF || '',
        database: process.env.MYSQL_DB_ZEUS_BI_SIAF || 'ejemplo'
    },
    mysql_zeus_siga: {
        host: process.env.MYSQL_HOST || 'localhost',
        port: process.env.MYSQL_PORT || 3306,
        user: process.env.MYSQL_USER_ZEUS_BI_SIGA || 'root',
        password: process.env.MYSQL_PASSWORD_ZEUS_BI_SIGA || '',
        database: process.env.MYSQL_DB_ZEUS_BI_SIGA || 'ejemplo'
    }
}